import platform
import os 
import trex

dir_path = os.path.dirname(os.path.realpath(__file__))
print("-" * 40)
print(platform.system())
print(platform.release())
print(platform.version())
print("-" * 40)
print("python " + platform.python_version())
print("-" * 40)
print(dir_path)
