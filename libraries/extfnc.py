import time

class extfnc:

	def first_fnc(self, my_var):
		ret = "-" + my_var + "-"
		print(ret)
		print("###" + my_var)
		assert (len(my_var)>0), "String supposed not to be empty!"
		return ret
        
	def get_unique_filename(self, my_file):
		z = my_file.split("/")
		x = z[-1].split(".")
		x[0] += time.strftime("%Y%m%d%H%M%S") 
		return '.'.join(x)
