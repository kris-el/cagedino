# cagedino

Install plugin: Remote - Containers

Open the folder of the repository in VS Code

Press F1 and input the text: "Remote-containers: Reopen in container"

Now you can run the project in debuger tab

Uncompress vscode.7z to have the debugger ready for robot and python

# Warning

- Check line endings Unix scripts has to be LF (git rules configured store files in Unix style format, but conversion could applied when you download it).

- Check virtual environment in use in VS Code statusbar (should be trex)
