#!/bin/bash

LOG_FILE=boot.txt
VER=v2.71
ENVNAME=trex
DISTRO=$(cat /etc/*-release | grep PRETTY_NAME)
VENVSFOLDER=~/.virtualenvs/
TREXSETUPFOLDER=/tmp/
# VENVSFOLDER="../"
VENVCMD="python -m virtualenv"
# VENVCMD="virtualenv"

echo "---------------------------------------- >>> Begin" >> $LOG_FILE

date > $LOG_FILE
pwd >> $LOG_FILE
echo ~ >> $LOG_FILE
echo $DISTRO >> $LOG_FILE

echo "---------------------------------------- >>> Python framework" >> $LOG_FILE

pip install virtualenv >> $LOG_FILE

echo "---------------------------------------- >>> Setup env " >> $LOG_FILE

mkdir -p $VENVSFOLDER
echo $VENVCMD $VENVSFOLDER$ENVNAME  >> $LOG_FILE
$VENVCMD $VENVSFOLDER$ENVNAME  >> $LOG_FILE

. $VENVSFOLDER$ENVNAME/bin/activate
pip install scapy >> $LOG_FILE
deactivate

echo "---------------------------------------- >>> Copy folders to Env" >> $LOG_FILE

PYTHON_FOLDER=$(find $VENVSFOLDER$ENVNAME/lib/ -maxdepth 1 -iname python* | head -1)
echo $PYTHON_FOLDER >> $LOG_FILE

cp -R ${TREXSETUPFOLDER}trex_client/external_libs/* $PYTHON_FOLDER/site-packages 2>> $LOG_FILE
cp -R ${TREXSETUPFOLDER}trex_client/interactive/trex_stl_lib $PYTHON_FOLDER/site-packages 2>> $LOG_FILE
cp -R ${TREXSETUPFOLDER}trex_client/interactive/trex $PYTHON_FOLDER/site-packages 2>> $LOG_FILE
cp -R ${TREXSETUPFOLDER}trex_client/stf/trex_stf_lib $PYTHON_FOLDER/site-packages 2>> $LOG_FILE

echo "---------------------------------------- >>> Edit conf files" >> $LOG_FILE

# Inser at row 12
sed -i.bak '12i\    os.environ["TREX_EXT_LIBS"] = os.path.normpath(os.path.join(CURRENT_PATH, os.pardir))\' $PYTHON_FOLDER/site-packages/trex/__init__.py 2>> $LOG_FILE
# Replace first occurence
sed -i.bak '0,/external_libs/{s/external_libs/site-packages/}' $PYTHON_FOLDER/site-packages/trex_stf_lib/outer_packages.py 2>> $LOG_FILE
# Replace ll occurences
#sed -i.bak s/"external_libs"/"site-packages"/ $PYTHON_FOLDER/site-packages/trex_stf_lib/outer_packages.py

echo "---------------------------------------- >>> Fix Scapy" >> $LOG_FILE

SCAPYVER=$(ls $PYTHON_FOLDER/site-packages/ | grep scapy- | grep -v dist | head -1) # scapy-2.3.1
echo $SCAPYVER >> $LOG_FILE
mv $PYTHON_FOLDER/site-packages/$SCAPYVER/python3/scapy $PYTHON_FOLDER/site-packages/$SCAPYVER/python3/orig_scapy 2>> $LOG_FILE
ln -s $PYTHON_FOLDER/site-packages/scapy $PYTHON_FOLDER/site-packages/$SCAPYVER/python3/scapy 2>> $LOG_FILE

echo "---------------------------------------- >>> Fix Simply" >> $LOG_FILE

SIMPLYVER=$(ls $PYTHON_FOLDER/site-packages/ | grep simpy- | head -1) # simpy-3.0.10
echo $SIMPLYVER >> $LOG_FILE
ln -s $PYTHON_FOLDER/site-packages/$SIMPLYVER $PYTHON_FOLDER/site-packages/simpy 2>> $LOG_FILE

echo "---------------------------------------- >>> Extra packages" >> $LOG_FILE

. $VENVSFOLDER$ENVNAME/bin/activate
pip install --no-cache-dir -r requirements.txt >> $LOG_FILE
deactivate


echo "---------------------------------------- >>> Clean install folders" >> $LOG_FILE

# rm -fr ${TREXSETUPFOLDER}trex_client 2>> $LOG_FILE

echo "---------------------------------------- >>> End" >> $LOG_FILE